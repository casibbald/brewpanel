#!/usr/bin/env bash

# The following detects the base dir location of the running script, in the event it runs on jenkins or some other env
# where temp dirs may be setup on symlinks etc.
SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do
  DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE"
done
DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"


#pip install -r requirements.txt
rm -f db.sqlite3
rm -rf djangok8s/auth/migrations/*_auto*.py
rm -rf djangok8s/migrations/*_auto*.py

#cp djangok8s/migrations/template/0001_initial.py djangok8s/migrations/

#python manage.py makemigrations portalauth --traceback
#python manage.py migrate --traceback
#python manage.py makemigrations portalapi --traceback
#python manage.py migrate --traceback
python manage.py runserver