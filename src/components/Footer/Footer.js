import React, {Component} from 'react';

class Footer extends Component {
  render() {
    return (
      <footer className="app-footer">
        <span><a href="https://european.brewery.systems">European Brewery Systems</a> &copy; 2018.</span>
      </footer>
    )
  }
}

export default Footer;
